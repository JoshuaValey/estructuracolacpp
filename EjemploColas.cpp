#include <iostream>
#include <stdlib.h>

using namespace std;

struct Nodo
{
	int dato;
	Nodo* siguiente;
};

void PushCola(Nodo*&, Nodo*&, int);
void PopCola(Nodo*&, Nodo*&, int&);
bool colaVacia(Nodo*);

int main()
{
	Nodo* frente = NULL;
	Nodo* fin = NULL;

	//Rellenar cola. 
	int dato;
	cout << "Digite un numero: ";
	cin >> dato;
	PushCola(frente, fin, dato);
	
	cout << "Digite un numero: ";
	cin >> dato;
	PushCola(frente, fin, dato);
	
	cout << "Digite un numero: ";
	cin >> dato;
	PushCola(frente, fin, dato);

	//Elminar los elementos de la cola si hay elementos. 
	cout << "\nSacando los nodos de la cola:\n";
	while (frente != NULL)
	{
		PopCola(frente, fin, dato);

		if (frente != NULL)
		{
			cout << dato << " , ";
		}
		else
		{
			cout << dato << ".";
		}
	}
}

/*
	Para incertar elementos en una cola hay tres pasos.
	Paso 1. Recervar espacio en memoria para almacenar un nodo.
	Paso 2. Asignar ese nuevo nodo al dato que queremos insertar.
	Paso 3. Asignar los punteros frente y fin hacia el nuevo nodo.
*/
void PushCola(Nodo*& frente, Nodo*& fin, int valor)
{
	Nodo* nuevoNodo = new Nodo();//Paso 1.
	//Paso 2.
	nuevoNodo->dato = valor;
	nuevoNodo->siguiente = NULL;
	if (colaVacia(frente))
		frente = nuevoNodo;
	else
		fin->siguiente = nuevoNodo;

	fin = nuevoNodo;

	cout << "\nElemento " << valor << " insertado a la cola correctamente\n";

}

/*
	Pasos para sacar elementos de una Cola. 
	1. Obtener el Valor del Nodo. 
	2. Crear un Nodo aux y asignarle el frente de la cola. 
	3. Eliminar el nodo del frente de la cola. 

*/
void PopCola(Nodo* &frente, Nodo* &fin, int &valor)
{
	valor = frente->dato;//Paso 1. 
	Nodo* aux = frente; //Paso 2. 


	if (frente == fin) //Si hay un solo nodo en la cola
	{
		frente = NULL;
		fin = NULL;
	}
	else
	{
		frente = frente->siguiente;
	}

	delete aux;


}


//Funcion para determinar si la cola está vacía o no.
bool colaVacia(Nodo* frente)
{
	return (frente == NULL) ? true : false;
}

